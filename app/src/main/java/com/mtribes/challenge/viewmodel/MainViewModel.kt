package com.mtribes.challenge.viewmodel

import android.annotation.SuppressLint
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.mtribes.challenge.model.CarsListResponse
import com.mtribes.challenge.repository.CarsRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class MainViewModel @Inject constructor(): ViewModel() {

    var carsListResponseLiveData = MutableLiveData<CarsListResponse>()
    var errorLiveData = MutableLiveData<String>()

    @SuppressLint("CheckResult")
    fun fetchCarsList() {
        val carsRepository = CarsRepository()
        carsRepository.getCarsObservable().subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ response: CarsListResponse? ->
                carsListResponseLiveData.value = response
            }, { error: Throwable? ->
                errorLiveData.value = error?.localizedMessage
            })
    }
}