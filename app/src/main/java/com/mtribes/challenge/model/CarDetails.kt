package com.mtribes.challenge.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class CarDetails() : Parcelable {

    @SerializedName("address")
    var address: String? = null
    @SerializedName("coordinates")
    var coordinates: List<Double>? = null
    @SerializedName("engineType")
    var engineType: String? = null
    @SerializedName("exterior")
    var exterior: String? = null
    @SerializedName("fuel")
    var fuel: Int = 0
    @SerializedName("interior")
    var interior: String? = null
    @SerializedName("name")
    var name: String? = null
    @SerializedName("vin")
    val vin: String? = null

    constructor(parcel: Parcel) : this() {
        address = parcel.readString()
        engineType = parcel.readString()
        exterior = parcel.readString()
        fuel = parcel.readInt()
        interior = parcel.readString()
        name = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(address)
        parcel.writeString(engineType)
        parcel.writeString(exterior)
        parcel.writeInt(fuel)
        parcel.writeString(interior)
        parcel.writeString(name)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CarDetails> {
        override fun createFromParcel(parcel: Parcel): CarDetails {
            return CarDetails(parcel)
        }

        override fun newArray(size: Int): Array<CarDetails?> {
            return arrayOfNulls(size)
        }
    }
}