package com.mtribes.challenge.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class CarsListResponse() : Parcelable {
    @SerializedName("placemarks")
    var carsList: List<CarDetails>? = null

    constructor(parcel: Parcel) : this() {
        carsList = parcel.createTypedArrayList(CarDetails)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeTypedList(carsList)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CarsListResponse> {
        override fun createFromParcel(parcel: Parcel): CarsListResponse {
            return CarsListResponse(parcel)
        }

        override fun newArray(size: Int): Array<CarsListResponse?> {
            return arrayOfNulls(size)
        }
    }
}