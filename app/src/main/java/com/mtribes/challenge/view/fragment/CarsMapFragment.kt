package com.mtribes.challenge.view.fragment

import android.Manifest
import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.mtribes.challenge.R
import com.mtribes.challenge.model.CarsListResponse
import com.mtribes.challenge.utils.LocationUtils
import com.mtribes.challenge.view.activity.MainActivity


class CarsMapFragment : Fragment(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener,
    GoogleMap.OnInfoWindowCloseListener {

    private var carsListResponse: CarsListResponse? = null

    private var isSingleMarker: Boolean = false

    private var map: GoogleMap? = null

    private val permissionRequestId: Int = 1

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_cars_map, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        extractData()
        initMap()
    }

    private fun extractData() {
        carsListResponse = arguments?.getParcelable(MainActivity.responseKey)
    }

    private fun initMap() {
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(map: GoogleMap?) {
        this.map = map
        map?.setOnMarkerClickListener(this)
        map?.setOnInfoWindowCloseListener(this)
        checkForLocation()
        drawMarkers()
    }

    @SuppressLint("MissingPermission")
    private fun checkForLocation() {
        if (LocationUtils.isLocationPermissionGranted(context!!)) {
            map?.isMyLocationEnabled = true
        } else {
            requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), permissionRequestId)
        }
    }

    private fun drawMarkers() {
        map?.clear()
        val builder = LatLngBounds.Builder()
        carsListResponse?.carsList?.iterator()?.forEach {
            if (it.coordinates != null) {
                var latLng = LatLng(it.coordinates?.get(1)!!, it.coordinates?.get(0)!!)
                map?.addMarker(MarkerOptions().position(latLng).title(it.name))
                builder.include(latLng)
            }
        }
        val bounds = builder.build()
        val cameraUpdateFactory = CameraUpdateFactory.newLatLngBounds(bounds, 0)
        map?.animateCamera(cameraUpdateFactory)
    }

    override fun onMarkerClick(marker: Marker?): Boolean {
        if (!isSingleMarker) {
            drawMarker(marker!!)
        }
        isSingleMarker = !isSingleMarker
        return true
    }

    private fun drawMarker(marker: Marker) {
        map?.clear()
        map?.addMarker(MarkerOptions().position(marker.position).title(marker.title))?.showInfoWindow()
        map?.animateCamera(CameraUpdateFactory.newLatLng(marker.position))

    }

    override fun onInfoWindowClose(p0: Marker?) {
        drawMarkers()
    }

    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == permissionRequestId) {
            if (LocationUtils.isLocationPermissionGranted(context!!)) {
                map?.isMyLocationEnabled = true
            }
        }
    }
}