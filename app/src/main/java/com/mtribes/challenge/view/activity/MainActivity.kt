package com.mtribes.challenge.view.activity

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.mtribes.challenge.R
import com.mtribes.challenge.di.ViewModelFactory
import com.mtribes.challenge.model.CarsListResponse
import com.mtribes.challenge.view.fragment.CarsListFragment
import com.mtribes.challenge.view.fragment.CarsMapFragment
import com.mtribes.challenge.viewmodel.MainViewModel
import com.mtribes.challenge.viewmodel.MainViewModel2
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var vmFactory: ViewModelFactory<MainViewModel2>


    companion object {
        val responseKey = "CarsListResponse"
    }

    private var viewModel: MainViewModel2? = null
    private var carsListResponse: CarsListResponse? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ViewModelProviders.of(this).get(MainViewModel::class.java)
        AndroidInjection.inject(this)
        setContentView(R.layout.activity_main)
        initUI()
        createViewModelAndSubscribeToLiveData()
    }

    private fun initUI() {
        tabLayout.addOnTabSelectedListener(object : TabLayout.BaseOnTabSelectedListener<TabLayout.Tab> {
            override fun onTabReselected(p0: TabLayout.Tab?) {
            }

            override fun onTabUnselected(p0: TabLayout.Tab?) {
            }

            override fun onTabSelected(p0: TabLayout.Tab?) {
                when (p0?.position) {
                    0 -> {
                        displayFragment(CarsListFragment())
                    }
                    1 -> {
                        displayFragment(CarsMapFragment())
                    }
                }
            }

        })
    }

    private fun createViewModelAndSubscribeToLiveData() {
        viewModel = vmFactory.create(MainViewModel2::class.java)
        viewModel?.carsListResponseLiveData?.observe(this, Observer<CarsListResponse> { showResults(it) })
        viewModel?.errorLiveData?.observe(this, Observer<String> { showError(it) })
        viewModel?.fetchCarsList()
    }


    private fun showResults(carsListResponse: CarsListResponse?) {
        this.carsListResponse = carsListResponse
        loadingProgressBar.visibility = View.GONE
        resultsLayout.visibility = View.VISIBLE
        displayFragment(CarsListFragment())
    }

    private fun displayFragment(fragment: Fragment?) {
        if (fragment != null) {
            var bundle = Bundle()
            bundle.putParcelable(responseKey, carsListResponse)
            fragment.arguments = bundle
            supportFragmentManager.beginTransaction().replace(R.id.containerFrameLayout, fragment).commit()
        }
    }

    private fun showError(error: String?) {
        loadingProgressBar.visibility = View.GONE
        errorTextView.visibility = View.VISIBLE
        errorTextView.text = error
    }
}
