package com.mtribes.challenge.view.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.mtribes.challenge.R
import com.mtribes.challenge.model.CarDetails

class CarsListAdapter constructor(private var carsList: List<CarDetails>?) :
    RecyclerView.Adapter<CarsListAdapter.CarViewHolder>() {


    override fun onCreateViewHolder(viewGroup: ViewGroup, p1: Int): CarViewHolder {
        var view = LayoutInflater.from(viewGroup.context).inflate(R.layout.car_item_layout, viewGroup, false)
        return CarViewHolder(view)
    }

    override fun getItemCount(): Int {
        return if (carsList != null)
            carsList?.size!!
        else
            0
    }

    override fun onBindViewHolder(viewHolder: CarViewHolder, position: Int) {
        viewHolder.nameTextView.text = carsList?.get(position)?.name
        viewHolder.addressTextView.text = carsList?.get(position)?.address
        viewHolder.engineTypeTextView.text = carsList?.get(position)?.engineType
        viewHolder.fuelTextView.text = carsList?.get(position)?.fuel.toString()
        viewHolder.interiorTextView.text = carsList?.get(position)?.interior
        viewHolder.exteriorTextView.text = carsList?.get(position)?.exterior
    }

    class CarViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var nameTextView: TextView = itemView.findViewById(R.id.nameTextView)
        var addressTextView: TextView = itemView.findViewById(R.id.addressTextView)
        var engineTypeTextView: TextView = itemView.findViewById(R.id.engineTypeTextView)
        var fuelTextView: TextView = itemView.findViewById(R.id.fuelTextView)
        var interiorTextView: TextView = itemView.findViewById(R.id.interiorTextView)
        var exteriorTextView: TextView = itemView.findViewById(R.id.exteriorTextView)
    }
}