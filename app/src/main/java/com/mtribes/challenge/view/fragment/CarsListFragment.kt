package com.mtribes.challenge.view.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mtribes.challenge.R
import com.mtribes.challenge.model.CarsListResponse
import com.mtribes.challenge.view.activity.MainActivity
import com.mtribes.challenge.view.adapter.CarsListAdapter

class CarsListFragment : Fragment() {

    private var carsRecyclerView: RecyclerView? = null
    private var carsListResponse: CarsListResponse? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var rootView = inflater.inflate(R.layout.cars_list_fragment, container, false)
        carsRecyclerView = rootView.findViewById(R.id.carsRecyclerView)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        extractData()
        displayData()
    }

    private fun extractData() {
        carsListResponse = arguments?.get(MainActivity.responseKey) as CarsListResponse?
    }


    private fun displayData() {
        carsRecyclerView?.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        var carsListAdapter = CarsListAdapter(carsListResponse?.carsList)
        carsRecyclerView?.adapter = carsListAdapter
    }
}