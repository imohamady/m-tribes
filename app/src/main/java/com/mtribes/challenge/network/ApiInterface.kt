package com.mtribes.challenge.network

import com.mtribes.challenge.model.CarsListResponse
import io.reactivex.Observable
import retrofit2.http.GET

interface ApiInterface {

    @GET("locations")
    fun getCarsListObservable(): Observable<CarsListResponse>
}