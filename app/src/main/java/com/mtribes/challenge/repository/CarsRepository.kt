package com.mtribes.challenge.repository

import com.mtribes.challenge.model.CarsListResponse
import com.mtribes.challenge.network.ApiClient
import com.mtribes.challenge.network.ApiInterface
import io.reactivex.Observable

class CarsRepository {

    fun getCarsObservable(): Observable<CarsListResponse> {
        var apiClient = ApiClient.getClient().create(ApiInterface::class.java)
        return apiClient.getCarsListObservable()
    }
}